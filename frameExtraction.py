import cv2
import glob 
import pathlib
import os
# Search Folders in Root Folder, aka Dataset Root Folder

def readFilesInFolder(folderName):
    list = glob.glob(folderName+"/*")
    return list

def extractFramesFromVideo(fileName):
    cap = cv2.VideoCapture(fileName)
    fileNameTrunc = fileName[:-4]
    framesFolder = fileNameTrunc+"_Frames"
    basename = os.path.basename(fileNameTrunc)
    if not os.path.exists (framesFolder):
        os.mkdir(framesFolder)
    count = 0
    framesToSkip = 5
    while True:
        success,image = cap.read()
        if(success == False):
            break
        if(count % framesToSkip == 0):
            cv2.imwrite(framesFolder+"/"+basename+"Frame_"+str(int(count/framesToSkip))+".jpg", image) 
        count +=1 
    cap.release()
    cv2.destroyAllWindows()

def extractFramesFromFolder(folderName):
    fileList = readFilesInFolder(folderName)
    os.chdir(folderName)
    print(os.getcwd())
    for file in fileList:
        print(file)
        extractFramesFromVideo(file)
        os.remove(file)

def divide():
    folderList = readFilesInFolder("/home/marco/ProgettoFVAB/XM2VTS")
    print(folderList)
    for folder in folderList:    
        extractFramesFromFolder(folder)

divide()
